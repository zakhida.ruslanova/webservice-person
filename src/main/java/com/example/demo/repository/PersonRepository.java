package com.example.demo.repository;

import com.example.demo.entity.PersonEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonRepository extends CrudRepository< PersonEntity, String > {

    @Override
    List< PersonEntity > findAll();
}
