package com.example.demo.repository;

import com.example.demo.entity.GruppEntity;
import org.springframework.data.repository.CrudRepository;

public interface GruppRepository extends CrudRepository< GruppEntity, String > {
}
