package com.example.demo.service;

import com.example.demo.dto.CreatePerson;
import com.example.demo.entity.GruppEntity;
import com.example.demo.entity.PersonEntity;
import com.example.demo.exceptions.GroupClientException;
import com.example.demo.exceptions.PersonException;
import com.example.demo.remote.RemoteGroup;
import com.example.demo.repository.GruppRepository;
import com.example.demo.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.Stream;

@Service
public class PersonService {

    PersonRepository personRepository;
    GruppRepository gruppRepository;
    RemoteGroup remoteGroup;

    public PersonService(PersonRepository personRepository, GruppRepository gruppRepository, RemoteGroup remoteGroup) {
        this.personRepository = personRepository;
        this.gruppRepository = gruppRepository;
        this.remoteGroup = remoteGroup;
    }

    public Stream< PersonEntity > getAllPersons() {
        return personRepository.findAll().stream();
    }

    public PersonEntity createPerson(CreatePerson createPerson) {
        return personRepository.save(new PersonEntity(
                createPerson.getId(),
                createPerson.getFirstName(),
                createPerson.getLastName(),
                createPerson.getUsername(),
                createPerson.getPassword(),
                new ArrayList<>(),
                new ArrayList<>()));
    }

    public PersonEntity addPersonToGrupp(String personId, String gruppId) throws PersonException, GroupClientException {
        PersonEntity person = personRepository.findById(personId).orElseThrow(() -> new PersonException("Person is not found"));
        GruppEntity grupp = remoteGroup.getGroupById(gruppId).block();
        person.addGrupp(grupp);
        grupp.addPerson(person);
        personRepository.save(person);
        gruppRepository.save(grupp);
        return person;
    }

    public PersonEntity getPersonById(String id) throws PersonException {
        return personRepository.findById(id).orElseThrow(() -> new PersonException("Person id " + id + " not found"));
    }
}
