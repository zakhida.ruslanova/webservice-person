package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreatePerson {
    String id;
    String firstName;
    String lastName;
    String username;
    String password;

    @JsonCreator
    public CreatePerson(@JsonProperty String id,
                        @JsonProperty String firstName,
                        @JsonProperty String lastName,
                        @JsonProperty String userName,
                        @JsonProperty String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = userName;
        this.password = password;
    }
}
