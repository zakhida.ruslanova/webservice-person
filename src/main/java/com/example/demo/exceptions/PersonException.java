package com.example.demo.exceptions;

public class PersonException extends Exception {
    public PersonException(String message) {
        super(message);
    }
}
