package com.example.demo.entityToDto;

import com.example.demo.domain.PersonDTO;
import com.example.demo.entity.PersonEntity;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public interface PersonEntityToDto {
    default PersonDTO PersonEntityToDto(PersonEntity personEntity) {
        return new PersonDTO(
                personEntity.getId(),
                personEntity.getFirstName(),
                personEntity.getLastName(),
                personEntity.getUsername(),
                personEntity.getPassword(),
                personEntity.getGrupper().stream().map(
                                personGroupEntity ->
                                        personGroupEntity.getId())
                        .collect(Collectors.toList()));
    }
}
