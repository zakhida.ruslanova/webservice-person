package com.example.demo.remote;

import com.example.demo.entity.GruppEntity;
import com.example.demo.exceptions.GroupClientException;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class RemoteGroup {

    public Mono< GruppEntity > getGroupById(String groupId) throws GroupClientException {
        try {
            WebClient webClient = WebClient.create("http://localhost:" + 8081 + "/grupper/getGruppById/" + groupId);

            return webClient.get()
                    .retrieve()
                    .bodyToMono(GruppEntity.class);
        } catch (Exception e) {
            throw new GroupClientException(e);
        }
    }
}
