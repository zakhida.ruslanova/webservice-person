package com.example.demo.controller;

import com.example.demo.domain.PersonDTO;
import com.example.demo.dto.CreatePerson;
import com.example.demo.entity.PersonEntity;
import com.example.demo.entityToDto.PersonEntityToDto;
import com.example.demo.exceptions.GroupClientException;
import com.example.demo.exceptions.PersonException;
import com.example.demo.repository.GruppRepository;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("/persons")
@AllArgsConstructor
public class PersonController implements PersonEntityToDto {

    PersonRepository personRepository;
    GruppRepository gruppRepository;
    PersonService personService;

    @GetMapping("/getAll")
    public List< PersonDTO > getPersons() {
        return personService.getAllPersons().map(this::PersonEntityToDto).collect(Collectors.toList());
    }

    @PostMapping("/createPerson")
    public PersonDTO createPerson(@RequestBody CreatePerson createPerson) {
        return PersonEntityToDto(personService.createPerson(createPerson));
    }

    @PutMapping(path = "/update/{personId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PersonDTO updatePersonById(@PathVariable String personId, @RequestBody CreatePerson createPerson) {
        PersonEntity foundPerson = personRepository.findById(personId).get();
        foundPerson.setFirstName(createPerson.getFirstName());
        foundPerson.setLastName(createPerson.getLastName());
        personRepository.save(foundPerson);
        return PersonEntityToDto(foundPerson);
    }

    @DeleteMapping("/delete/{personId}")
    public void deletePerson(@PathVariable String personId) {
        final Optional< PersonEntity > personDTO = personRepository.findById(personId);
        personRepository.deleteById(personId);
    }

    @GetMapping("/getPersonById/{id}")
    public PersonDTO getPersonById(@PathVariable String id) throws PersonException {
        return PersonEntityToDto(personService.getPersonById(id));
    }

    @GetMapping("/addPersonToGrupp/{personId}/{gruppId}")
    public PersonDTO addPersonToGrupp(@PathVariable String personId, @PathVariable String gruppId) throws PersonException, GroupClientException {
        return PersonEntityToDto(personService.addPersonToGrupp(personId, gruppId));
    }
}
