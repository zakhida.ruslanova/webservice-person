package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class PersonDTO {
    private String id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private List< String > grupper;

    @JsonCreator
    public PersonDTO(@JsonProperty("id") String id,
                     @JsonProperty("firstName") String firstName,
                     @JsonProperty("lastName") String lastName,
                     @JsonProperty("username") String username,
                     @JsonProperty("password") String password,
                     @JsonProperty("grupper") List< String > grupper) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.grupper = grupper;
    }
}
